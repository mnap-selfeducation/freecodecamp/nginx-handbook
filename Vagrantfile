# -*- mode: ruby -*-
# vi: set ft=ruby :
ENV['VAGRANT_DEFAULT_PROVIDER'] = 'libvirt'

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.hostname = "nginx-handbook-box"
  #config.vm.box = "generic/ubuntu2004"
  config.vm.box = "debian/bullseye64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "10.0.6.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"
  #config.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true
  config.vm.synced_folder ".", "/vagrant",
    id: "vagrant-root",
    type: "nfs",
    nfs_version: 4,
    nfs_udp: false
  config.vm.synced_folder "./static-demo", "/srv/www/static-demo",
    id: "static-demo",
    type: "nfs",
    nfs_version: 4,
    nfs_udp: false
  config.vm.synced_folder "./node-js-demo", "/srv/www/node-js-demo",
    id: "node-js-demo",
    type: "nfs",
    nfs_version: 4,
    nfs_udp: false
  config.vm.synced_folder "./php-demo", "/srv/www/php-demo",
    id: "php-demo",
    type: "nfs",
    nfs_version: 4,
    nfs_udp: false
  config.vm.synced_folder "./load-balancer-demo", "/srv/www/load-balancer-demo",
    id: "load-balancer-demo",
    type: "nfs",
    nfs_version: 4,
    nfs_udp: false

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.
  config.vm.provider "virtualbox" do |vb|
    vb.cpus = 2
    vb.memory = "1024"
    vb.name = "nginx-handbook"
  end

  config.vm.provider :libvirt do |libvirt|
    libvirt.storage_pool_name = "default"
    libvirt.driver = "kvm"
    libvirt.cpus = 2
    libvirt.memory = 1024
  end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    echo
    echo "*** INFO *** Install Nginx and system utilities."
    echo
    apt update && sudo apt upgrade --assume-yes
    apt install --assume-yes curl gnupg nginx
    systemctl status nginx || systemctl start nginx
    echo
    echo
    echo "*** INFO *** Install Node.js from NodeSource PPA."
    echo "*** INFO *** Install PHP Fast Process Manager interpreter."
    echo
    KEYRING="/etc/apt/trusted.gpg.d/nodesource.gpg"
    VERSION="node_14.x"
    DISTRO="$(lsb_release -s -c)"
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | gpg --dearmor | sudo tee $KEYRING >/dev/null
    echo "deb [signed-by=$KEYRING] https://deb.nodesource.com/$VERSION $DISTRO main" | sudo tee /etc/apt/sources.list.d/nodesource.list
    echo "deb-src [signed-by=$KEYRING] https://deb.nodesource.com/$VERSION $DISTRO main" | sudo tee -a /etc/apt/sources.list.d/nodesource.list
    apt update && apt install --assume-yes nodejs php-fpm
    echo
    echo "*** INFO *** Install PM2 globally."
    echo
    npm install --global pm2
    echo
    echo
    echo "*** INFO *** Cleanup."
    echo
    apt clean --assume-yes && apt autoremove --assume-yes
  SHELL
end
